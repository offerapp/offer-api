use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :offer_api, OfferApi.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :offer_api, OfferApi.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "offer",
  password: "offer",
  hostname: "0.0.0.0",
  database: "offer_api_test",
  port: 3399,
  pool: Ecto.Adapters.SQL.Sandbox
