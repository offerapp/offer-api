# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :offer_api,
  ecto_repos: [OfferApi.Repo]

# Configures the endpoint
config :offer_api, OfferApi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "cR359ngSfVE6+7j8DJ5HDB9DMYczYhn78DdVikAh0Kq1dk/Htss420VkviH50w1E",
  render_errors: [view: OfferApi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: OfferApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :ex_admin,
  repo: OfferApi.Repo,
  module: OfferApi,
  modules: [
    OfferApi.ExAdmin.Dashboard,
    OfferApi.ExAdmin.Promotion,
    OfferApi.ExAdmin.Tag,
    OfferApi.ExAdmin.Site,
    OfferApi.ExAdmin.Organization,
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :xain, :after_callback, {Phoenix.HTML, :raw}

