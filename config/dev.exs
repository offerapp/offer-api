use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :offer_api, OfferApi.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]]


# Watch static and templates for browser reloading.
config :offer_api, OfferApi.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :offer_api, OfferApi.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "mrkaspa",
  password: "",
  database: "core_dev",
  hostname: "localhost",
  pool_size: 10,
  types: OfferApi.PostgresTypes

config :facebook_messenger,
  facebook_page_token: "EAAYtXmYU6fABANClo5gMtxZCE0lfYfJgrrZAtpS2WPe5boxE8JCu2mfO7fZAGTMIm71fDZBCbFE0RIRZAIlDEyp23z071agytExzJaDGz9kjD2oguOHVolGq9GMmr2IDm0tlIQ2uLMAY6e9kvcZC7q0BMC8ZCcfUlC1iJs9c4ZCbYwZDZD",
  challenge_verification_token: "demo"
