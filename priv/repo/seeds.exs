# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     OfferApi.Repo.insert!(%OfferApi.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias OfferApi.{Repo, User, Organization, Site, Promotion}
alias OfferApi.User

org = Repo.insert!(%Organization{name: "Exito", legal_name: "Exito SAS", email: "demo@demo.com"})

site = Repo.insert!(%Site{organization_id: org.id, point: %Geo.Point{coordinates: {4.7072556257451, -74.04523733386}, srid: 4326}, address: "unicentro"})

promo = Repo.insert!(%Promotion{site_id: site.id, description: "Pescados al 10% de descuento"})

user = Repo.insert!(%User{fb_id: "", psid: "1543663848998169", name: "Michel", last_name: "Perez"})
