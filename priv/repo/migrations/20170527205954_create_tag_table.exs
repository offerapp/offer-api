defmodule OfferApi.Repo.Migrations.CreateTagTable do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :name, :string
      add :promotion_id, references(:promotions)
      add :user_id, references(:users)

      timestamps()
    end
  end
end
