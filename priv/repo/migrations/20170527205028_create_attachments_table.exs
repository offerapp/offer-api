defmodule OfferApi.Repo.Migrations.CreateAttachmentsTable do
  use Ecto.Migration

  def change do
    create table(:attachments) do
      add :url, :string
      add :promotion_id, references(:promotions)

      timestamps()
    end
  end
end
