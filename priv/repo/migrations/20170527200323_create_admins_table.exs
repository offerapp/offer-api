defmodule OfferApi.Repo.Migrations.CreateAdminsTable do
  use Ecto.Migration

  def change do
    create table(:admins) do
      add :name, :string
      add :last_name, :string
      add :email, :string
      add :phone_number, :string

      timestamps()
    end

    create unique_index(:admins, [:email, :phone_number])
  end
end
