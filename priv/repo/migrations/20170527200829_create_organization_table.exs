defmodule OfferApi.Repo.Migrations.CreateOrganizationTable do
  use Ecto.Migration

  def change do
    create table(:organizations) do
      add :name, :string
      add :email, :string
      add :legal_name, :string
      add :legal_id, :string
      add :admin_id, references(:admins)

      timestamps()
    end

    create unique_index(:organizations, [:legal_name, :legal_id])
  end
end
