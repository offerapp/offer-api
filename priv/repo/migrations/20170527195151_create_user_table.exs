defmodule OfferApi.Repo.Migrations.CreateUserTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :location, :geometry
      add :name, :string
      add :last_name, :string
      add :email, :string
      add :phone_number, :string
      add :fb_id, :string
      add :psid, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
