defmodule OfferApi.Repo.Migrations.CreateSitesTable do
  use Ecto.Migration

  def change do
    create table(:sites) do
      add :point, :geometry
      add :address, :string
      add :organization_id, references(:organizations)

      timestamps()
    end
  end
end
