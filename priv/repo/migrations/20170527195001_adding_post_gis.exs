defmodule OfferApi.Repo.Migrations.AddingPostGIS do
  use Ecto.Migration
  alias Ecto.Adapters.SQL
  alias OfferApi.Repo

  def change do
    SQL.query(Repo, "CREATE EXTENSION IF NOT EXISTS postgis")
  end
end
