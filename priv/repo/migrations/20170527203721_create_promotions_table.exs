defmodule OfferApi.Repo.Migrations.CreatePromotionsTable do
  use Ecto.Migration

  def change do
    create table(:promotions) do
      add :description, :text
      add :expiration_date, :utc_datetime

      add :site_id, references(:sites)

      timestamps()
    end
  end
end
