defmodule OfferApi.PromotionTest do
  use OfferApi.ModelCase

  alias OfferApi.Promotion

  @valid_attrs %{description: "some content", expiration_date: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010}, site_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Promotion.changeset(%Promotion{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Promotion.changeset(%Promotion{}, @invalid_attrs)
    refute changeset.valid?
  end
end
