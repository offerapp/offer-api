defmodule OfferApi.SiteTest do
  use OfferApi.ModelCase

  alias OfferApi.Site

  @valid_attrs %{address: "some content", lat: "some content", long: "some content", organization_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Site.changeset(%Site{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Site.changeset(%Site{}, @invalid_attrs)
    refute changeset.valid?
  end
end
