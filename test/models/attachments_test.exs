defmodule OfferApi.AttachmentsTest do
  use OfferApi.ModelCase

  alias OfferApi.Attachments

  @valid_attrs %{promotion_id: 42, url: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Attachments.changeset(%Attachments{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Attachments.changeset(%Attachments{}, @invalid_attrs)
    refute changeset.valid?
  end
end
