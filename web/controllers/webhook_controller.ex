defmodule OfferApi.WebHookController do
  use FacebookMessenger.Phoenix.Controller
  alias OfferApi.User

  def message_received(msg) do
    entry = hd(msg.entry)
    entry_id = entry.id
    message = hd(entry.messaging)
    IO.puts("------")
    IO.inspect(message)
    IO.puts("------")
    process_message(entry_id, message)
  end

  defp process_message(entry_id, %FacebookMessenger.Messaging{message: nil, sender: %FacebookMessenger.User{id: sender}}) do
    if User.user_exists?(sender) do
      FacebookMessenger.Sender.send(sender, "hola")
    else
      FacebookMessenger.Sender.send_login_button(sender, "Inicia sesion por favor", "https://www.example.com/authorize")
    end
  end

  defp process_message(entry_id, %FacebookMessenger.Messaging{message: %FacebookMessenger.Message{attachments: [%{"type" => "location", "payload" => %{"coordinates" => %{"lat" => lat, "long" => lon}}} | _]}, sender: %FacebookMessenger.User{id: sender}}) do
    res =
      sender
      |> User.get_user()
      |> User.update(%{location: %Geo.Point{coordinates: {lat, lon}, srid: 4326}})

    case res do
      {:ok, _} ->
        FacebookMessenger.Sender.send(sender, "Te enviaremos promocines cercanas a esa ubicacion")
      {:error, _} ->
        FacebookMessenger.Sender.send(sender, "Ocurrio un error al guardar la geolocalizacion")
    end
  end

  defp process_message(entry_id, %FacebookMessenger.Messaging{message: %FacebookMessenger.Message{text: text}, sender: %FacebookMessenger.User{id: sender}}) do
    send_response(entry_id, sender, text)
  end

  defp send_response(entry_id, sender, text) do
    {action, incomplete, parameters, messages} =
      entry_id
      |> ai_request(text)
      |> ai_process_response()

    if incomplete do
      message = messages |> hd() |> Map.get("speech")
      FacebookMessenger.Sender.send(sender, message)
    else
      OfferApi.Actions.do_action(sender, action, parameters, messages)
    end
  end

  def ai_request(entry_id, text) do
    body = Poison.encode!(%{
      query: text,
      lang: "es",
      timezone: "America/New_York",
      sessionId: entry_id
    })
    res = HTTPoison.post("https://api.api.ai/v1/query?v=20150910", body, [
      "Authorization": "Bearer 9aa97768fb754cdfbb01ed91943262d3",
      "Content-Type": "application/json; charset=utf-8"
    ])
  end

  def ai_process_response({:ok, %HTTPoison.Response{body: body, status_code: 200}}) do
    %{
      "result" => %{
        "action" => action,
        "actionIncomplete" => incomplete,
        "parameters" => parameters,
        "fulfillment" => %{
          "messages" => messages
        }
      }
    } = data = Poison.decode!(body)
    IO.inspect(data)
    {action, incomplete, parameters, messages}
  end

  def ai_process_response({:ok, %HTTPoison.Response{body: _body, status_code: status}}) do
    {:error, "Error http code #{status}"}
  end

  def ai_process_response({:error, %HTTPoison.Error{reason: reason}}) do
    {:error, reason}
  end
end
