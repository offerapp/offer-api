defmodule OfferApi.PageController do
  use OfferApi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
