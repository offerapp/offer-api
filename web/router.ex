defmodule OfferApi.Router do
  use OfferApi.Web, :router
  use FacebookMessenger.Phoenix.Router
  use ExAdmin.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", OfferApi do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", OfferApi do
    pipe_through :api

    facebook_routes "/webhook", WebHookController
  end

  scope "/admin", ExAdmin do
    pipe_through :browser
    admin_routes()
  end
end
