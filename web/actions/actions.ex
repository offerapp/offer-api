defmodule OfferApi.Actions do
  def do_action(sender, "ofer.register", params, messages) do
    #TODO guardar params
    FacebookMessenger.Sender.request_location(sender, "Dame tu ubicacion")
  end

  def do_action(sender, "input.welcome", params, messages) do
    message = messages |> hd() |> Map.get("speech")
    FacebookMessenger.Sender.send(sender, message)
  end

  def do_action(sender, "input.unknown", params, messages) do
  end
end
