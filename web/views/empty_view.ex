defmodule OfferApi.EmptyView do
  use OfferApi.Web, :view

  def render("empty.json", _) do
    %{}
  end
end
