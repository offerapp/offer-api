defmodule OfferApi.Attachment do
  use OfferApi.Web, :model

  schema "attachments" do
    field :url, :string

    belongs_to :promotion, OfferApi.Promotion

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:url, :promotion_id])
    |> validate_required([:url, :promotion_id])
  end
end
