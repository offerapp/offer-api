defmodule OfferApi.Site do
  use OfferApi.Web, :model
  import Geo.PostGIS
  alias __MODULE__

  schema "sites" do
    field :point, Geo.Geometry
    field :address, :string

    belongs_to :organization, OfferApi.Organization
    has_many :promotions, OfferApi.Promotion

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:lat, :long, :address, :organization_id])
    |> validate_required([:lat, :long, :address, :organization_id])
  end

  def nears(point) do
    query = from s in Site,
      join: p in assoc(s, :promotions),
      where: st_dwithin_in_meters(^point, s.point, 5000),
      select: p
    Repo.all(query)
  end
end
