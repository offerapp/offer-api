defmodule OfferApi.Tag do
  use OfferApi.Web, :model

  schema "tags" do
    field :name, :string

    belongs_to :user, OfferApi.Tag
    belongs_to :promotion, OfferApi.Promotion

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :user_id, :promotion_id])
    |> validate_required([:name, :user_id, :promotion_id])
  end
end
