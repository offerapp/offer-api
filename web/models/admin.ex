defmodule OfferApi.Admin do
  use OfferApi.Web, :model

  schema "admins" do
    field :name, :string
    field :last_name, :string
    field :email, :string
    field :phone_number, :string

    has_many :organizations, OfferApi.Site

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :last_name, :email, :phone_number])
    |> validate_required([:name, :last_name, :email, :phone_number])
  end
end
