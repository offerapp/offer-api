defmodule OfferApi.Organization do
  use OfferApi.Web, :model

  schema "organizations" do
    field :name, :string
    field :legal_name, :string
    field :email, :string

    has_many :sites, OfferApi.Site
    belongs_to :admin, OfferApi.Admin

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :legal_name, :email])
    |> validate_required([:name, :legal_name, :email])
  end
end
