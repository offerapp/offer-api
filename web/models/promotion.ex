defmodule OfferApi.Promotion do
  use OfferApi.Web, :model

  schema "promotions" do
    field :description, :string
    field :expiration_date, Ecto.DateTime

    has_many :attachments, OfferApi.Attachment
    belongs_to :site, OfferApi.Site

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:description, :expiration_date, :site_id])
    |> validate_required([:description, :site_id])
  end
end
