defmodule OfferApi.User do
  use OfferApi.Web, :model

  schema "users" do
    field :location, Geo.Geometry
    field :name, :string
    field :last_name, :string
    field :email, :string
    field :phone_number, :string
    field :fb_id, :string
    field :psid, :string

    has_many :tags, OfferApi.Tag
    has_many :promotions, OfferApi.Promotion

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :location])
    |> validate_required([:name])
  end

  def get_user(psid) do
    Repo.get_by(__MODULE__, psid: psid)
  end

  def user_exists?(psid) do
    get_user(psid) != nil
  end

  def update(user, params) do
    Repo.update(change(user, params))
  end
end
