defmodule OfferApi.Repo do
  use Ecto.Repo, otp_app: :offer_api
  use Scrivener, page_size: 10
end
