defmodule OfferApi.Worker do
  use GenServer
  require Logger
  alias OfferApi.{Repo, User, Site, Promotion}

  @page_token "EAAYtXmYU6fABAPXMoKJqtJZCiuHjuWg3BlVZCKaHaFFukZCG5cRe9GCmJImTekebMwqYjL3jnuSFgPuNT3bbIQKYJyjRC4j6KCdaPpxapLv9vexpbUsXPOeQofMylbR3WplyI4xqz9rAB7aLm7yZBIszYI5rS90bZAtPhzLAykQZDZD"

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    send_offers()
    Process.send_after(self(), :work, 1 * 60 * 1000) # In 2 hours
    {:ok, state}
  end

  def handle_info(:work, state) do
    # Do the work you desire here
    Logger.info("Sending promos")
    send_offers()

    # Start the timer again
    Process.send_after(self(), :work, 1 * 60 * 1000) # In 2 hours

    {:noreply, state}
  end

  def send_offers() do
    for %User{location: location, psid: psid} <- Repo.all(User) do
      for promo <- Site.nears(location) do
        send_noti(psid, promo)
      end
    end
  end

  def send_noti(psid, %Promotion{description: description}) do
    IO.puts("enviando promo")
    body = Poison.encode!(%{
      recipient: %{id: psid},
      message: %{text: "promocion: #{description}"}
    })
    res = HTTPoison.post("https://graph.facebook.com/v2.6/me/messages?access_token=#{@page_token}", body, ["Content-Type": "application/json; charset=utf-8"])
    IO.inspect(res)
  end
end
